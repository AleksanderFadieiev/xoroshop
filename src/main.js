import Vue from 'vue'
import store from '@/store';
import VueRouter from 'vue-router'
import router from './utils/routes.js'


Vue.use(VueRouter)

Vue.config.productionTip = false

new Vue({
  router,
  store,

}).$mount('#app')
