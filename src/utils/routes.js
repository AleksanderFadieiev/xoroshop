import Vue from 'vue'
import App from '../App.vue'
import VueRouter from 'vue-router'
import Edit from '../components/pages/edit.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/add', component: Edit },
  { path: '/', component: App },
  { path: '/edit/:id', component: Edit, props: true }
]
export default new VueRouter({
  mode: 'history',
  base: __dirname,
  routes
})
