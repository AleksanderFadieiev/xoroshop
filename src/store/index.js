import Vue from 'vue'
import Vuex from 'vuex';
import router from '../utils/routes';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    designs: [
      {
        title: 'Ostrov',
        id: 105,
        img: require('../assets/images/design1.jpg'),
      },
      {
        title: 'Flora',
        id: 104,
        img: require('../assets/images/design2.jpg'),
      },
      {
        title: 'SunWear',
        id: 103,
        img: require('../assets/images/design3.jpg'),
      },
      {
        title: 'Flora',
        id: 111,
        img: require('../assets/images/design1.jpg'),
      },
      {
        title: 'Ostrov',
        id: 107,
        img: require('../assets/images/design2.jpg'),
      },
      {
        title: 'Ostrov',
        id: 117,
        img: require('../assets/images/design3.jpg'),
      },
      {
        title: 'Ostrov',
        id: 11,
        img: require('../assets/images/design1.jpg'),
      },
      {
        title: 'Ostrov',
        id: 7,
        img: require('../assets/images/design2.jpg'),
      },
    ]
  },
  mutations: {
    add(state,el){
      let checkObject = state.designs.some((item)=>{
        return el.id == item.id
      })
      if(!checkObject){
        state.designs.push(el);
        alert('Элемент создан');
        router.push('/');
      } else {
        if(confirm('Вы уверены, что хотите перезаписать текущий элемент?')){
          state.designs.forEach((item)=>{
            if(item.id == el.id){
              item.title = el.title;
              item.img = el.img;
              return;
            }
          })
          alert('Элемент перезаписан');
          router.push('/');
        }
      }
    },
    remove(state,el){
      state.designs = state.designs.filter((item)=>{
        return item.id != el
      })
      alert('Элемент удален');
      router.push('/');
    }
  },
  actions: {
    add(context,el) {
      context.commit('add',el)
    },
    remove(context,el) {
      context.commit('remove',el)
    }
  },
  getters: {
    getAllCases: state => {
      return state.designs;
    },
    getCaseById: state => id => {
      return state.designs.find(item => item.id == id);
    }
  }
});
