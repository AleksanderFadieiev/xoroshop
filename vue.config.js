module.exports = {
  runtimeCompiler: true,
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "~@/assets/styles/config/settings.sass"`
      },
    }
  }
}
